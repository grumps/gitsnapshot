FROM python:3-slim-buster
#RUN groupadd -g 101 app \
#        && useradd --create-home \
#                    --home-dir /opt/app \
#                    --uid 100 --gid 101 \
#                    -s /usr/sbin/nologin \
#                    app
RUN pip install awscli && apt-get update -y && apt-get install -y git && apt-get clean
COPY gitbackup.sh /opt/app/gitbackup.sh
RUN chmod +x /opt/app/gitbackup.sh
#USER app
